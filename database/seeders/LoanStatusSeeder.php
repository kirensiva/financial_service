<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LoanStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = [
            'pending',
            'approved',
            'paid',
        ];
        foreach ($status as $oneStatus) {
            \App\Models\LoanStatus::factory()->create([
                'status' => $oneStatus
            ]);
        }
    }
}
