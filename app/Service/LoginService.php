<?php
namespace App\Service;

use App\Http\Resources\ApiLoginResource;
use App\Repository\LoginRepositoryInterface;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class LoginService
{
    public const INVALID_CRED_MSG = 'The provided credentials are incorrect.';
    /** @var LoginRepositoryInterface */
    protected $loginRepository = null;
    public function getLoginRepository()
    {
        return $this->loginRepository;
    }
    public function __construct(LoginRepositoryInterface $loginRepository)
    {
        $this->loginRepository = $loginRepository;
    }

    public function login($attributes)
    {
        $email = $attributes['email'];
        $password = $attributes['password'];
        $user = $this->getLoginRepository()->findOneBy(['email'=> $email]);
        if (empty($user)) {
            throw ValidationException::withMessages([
                'email' => [self::INVALID_CRED_MSG],
            ]);
        }
        $correct = Hash::check($password, $user->password);

        if (!$correct) {
            throw ValidationException::withMessages([
                'email' => [self::INVALID_CRED_MSG],
            ]);
        }
        $deviceId = 'default';
        $token = $user->createToken($deviceId);
        return new ApiLoginResource($token);
    }
}
