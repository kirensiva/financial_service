<?php

namespace App\Service\Loan;

class WeeklyLoanInstallment extends AbstractLoanInstallment
{
    // weekly duration
    public const INSTALLMENT_INTERVAL_FORMAT = 'P%sW';

    public function getDurationFormat()
    {
        return self::INSTALLMENT_INTERVAL_FORMAT;
    }
}
