<?php

namespace App\Service\Loan;

use App\Models\Loan;
use App\Repository\LoanInstallmentRepositoryInterface;
use DateInterval;
use DateTime;
use Exception;

abstract class AbstractLoanInstallment implements LoanInstallmentInterface
{
    public const INSTALLMENT_STATUS_PENDING = 'pending';
    public const INSTALLMENT_STATUS_PAID = 'paid';

    public function __construct(LoanInstallmentRepositoryInterface $loanInstallmentRepository)
    {
        $this->loanInstallmentRepository = $loanInstallmentRepository;
    }

    public function createInstallments(Loan $loan): Loan
    {
        $duration = $loan->installment_duration;
        $dates = $this->getInstallmentDates($duration);
        if (empty($dates)) {
            throw new Exception('Installment creation failed. Unable to generate dates');
        }
        $oneInstallmentAmount =  $this->getInstallmentAmount($loan);
        foreach ($dates as $date) {

            $attr['installment_date'] = $date;
            $attr['amount'] = $oneInstallmentAmount;
            $attr['amount_paid'] = 0;
            $attr['loan_id'] = $loan->id;

            $this->loanInstallmentRepository->storeLoanInstallment($loan, $attr);
        }
        return $loan;
    }

    public function payInstallment($installment, $attributes)
    {

        $amountPaid = $attributes['data']['attributes']['amount_paid'];
        if ($amountPaid < $installment->amount) {
            throw new Exception('Amount trying to pay is less than amount to be paid');
        }
        if ($installment->amount_paid >= $amountPaid) {
            throw new Exception('Already paid');
        }
        $installment->amount_paid = $amountPaid;
        $installment->status = self::INSTALLMENT_STATUS_PAID;
        $installment->save();
        return $installment;
    }

    protected function getInstallmentDates($tenure)
    {
        if ($tenure <= 0) {
            throw new Exception('Invalid installment duration');
        }
        $format = $this->getDurationFormat();
        $installmentDates = [];
        for ($i = 1; $i <= $tenure; $i++) {
            $durationFormat = sprintf($format, $i);
            $now = new DateTime();
            $interval = new DateInterval($durationFormat);
            $nextWeek = $now->add($interval);
            $installmentDates[] = $nextWeek->format('Y-m-d');
        }
        return $installmentDates;
    }

    protected function getInstallmentAmount(Loan $loan): float
    {
        $duration = $loan->installment_duration;
        $amount = $loan->amount;
        return   $amount / $duration;
    }

    abstract function getDurationFormat();
}
