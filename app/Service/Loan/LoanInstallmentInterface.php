<?php

namespace App\Service\Loan;

use App\Models\Loan;
use App\Models\LoanInstallment;

interface LoanInstallmentInterface
{
    public function createInstallments(Loan $loan);
    public function payInstallment(LoanInstallment $installment, array $attributes);
}
