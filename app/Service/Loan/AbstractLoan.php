<?php

namespace App\Service\Loan;

use App\Events\InstallmentPaid;
use App\Models\Loan;
use App\Models\LoanInstallment;
use App\Models\LoanStatus;
use App\Repository\LoanRepositoryInterface;
use App\Repository\LoanStatusRepositoryInterface;
use Illuminate\Support\Facades\Auth;

abstract class AbstractLoan

{
    public const LOAN_STATUS_PENDING = 'pending';
    public const LOAN_STATUS_APPROVED = 'approved';
    public const LOAN_STATUS_PAID = 'paid';

    protected $installmentService = null;
    protected $loanRepository = null;
    protected $loanStatusRepository = null;

    public function __construct(LoanInstallmentInterface $installmentService, LoanRepositoryInterface $loanRepository, LoanStatusRepositoryInterface $loanStatusRepository)
    {
        $this->installmentService = $installmentService;
        $this->loanRepository = $loanRepository;
        $this->loanStatusRepository = $loanStatusRepository;
    }

    public function getInstallmentService()
    {
        return $this->installmentService;
    }

    public function createLoan($attrs): Loan
    {
        $attrs['data']['attributes']['user_id'] = Auth::user()->id;
        $loan = $this->loanRepository->store($attrs);
        $this->getInstallmentService()->createInstallments($loan);
        $loan = $loan->refresh();
        return  $loan;
    }

    public function payInstallment(LoanInstallment $loanInstallment, $attributes): LoanInstallment
    {
        $loanInstallment = $this->getInstallmentService()->payInstallment($loanInstallment, $attributes);
        InstallmentPaid::dispatch($loanInstallment);
        return $loanInstallment;
    }

    public function updateLoan($attrs): Loan
    {
        $attrs['data']['attributes']['user_id'] = Auth::user()->id;
        $loan = $this->loanRepository->store($attrs);
        $loan = $loan->refresh();
        return  $loan;
    }

    public function approveLoan(Loan $loan)
    {
        $loanStatus =  $this->loanStatusRepository->findOneBy([
            'status' => LoanStatusRepositoryInterface::LOAN_STATUS_APPROVED
        ]);
        $loan->status()->associate($loanStatus);
        $loan->save();
        return $loan;
    }

    public function isLoanClosed(Loan $loan)
    {

        $installments = $loan->installments;
        $sum = 0;
        foreach ($installments as  $installment) {
            $amountPaid = $installment->amount_paid;
            $sum += $amountPaid;
        }
        $sum = round($sum);
        return $sum >= $loan->amount;
    }

    public function getLoans($user)
    {
        return $user->loans;
    }
}
