<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    protected function invalidJson($request, ValidationException $exception)
    {
        $formattedErrors = [];
        $errors = $exception->errors();
        $pointer = '/data/attributes/';
        if(!empty($errors)) {
            $tmpError = [];
            foreach($errors as $field => $message) {
                $tmpError['status'] = 422;
                $tmpError['source'] = [
                    'pointer'  =>  $pointer.$field
                ];
                $tmpError['title'] = $message;
                $tmpError['detail'] = $message;
                $formattedErrors[] = $tmpError;
            }
        }
        return response()->json($formattedErrors, $exception->status);
    }
}
