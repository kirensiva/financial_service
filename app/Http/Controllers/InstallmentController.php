<?php

namespace App\Http\Controllers;

use App\Http\Resources\LoanInstallmentResource;
use App\Http\Resources\LoanResource;
use App\Models\Loan;
use App\Models\LoanInstallment;
use Illuminate\Http\Request;
use App\Service\Loan\Loan as LoanService;

class InstallmentController extends Controller
{
    /**
     * @var LoanService
     */
    protected $loanService = null;
    public function __construct(LoanService $loanService)
    {
      
        $this->loanService = $loanService;
    }
    /**
     * @return LoanService
     */
    protected function getLoanService(): LoanService
    {
        return $this->loanService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function show(Loan $loan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function edit(Loan $loan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LoanInstallment $installment)
    {
        $attributes = $request->all();
        $loan = $this->getLoanService()->payInstallment($installment, $attributes);
        return new LoanInstallmentResource($loan);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Loan $loan)
    {
        //
    }
}
