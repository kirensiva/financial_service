<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Requests\ApiLoginRequest;
use App\Service\LoginService;

class ApiLoginController extends Controller
{
    /** @var LoginService */
    protected $loginService = null;
    public function __construct(LoginService $loginService)
    {
        $this->loginService = $loginService;
    }

    public function getLoginService()
    {
        return $this->loginService;
    }
    public function login(ApiLoginRequest $request)
    {
        $attributes = $request->only(['email', 'password']);
        $response = $this->getLoginService()->login($attributes);
        return response($response);
    }
}
