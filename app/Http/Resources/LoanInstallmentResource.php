<?php

namespace App\Http\Resources;

use App\Models\LoanInstallment;
use Illuminate\Http\Resources\Json\JsonResource;

class LoanInstallmentResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $loanInstallment = $this;
        return [
            'type' => LoanInstallment::class,
            'id' => $loanInstallment->id,
            'attributes' =>[
                'installment_date' => $loanInstallment->installment_date,
                'amount' => $loanInstallment->amount,
                'amount_paid' => $loanInstallment->amount_paid,
                'status' => $loanInstallment->status,
            ]
        ];
    }
}
