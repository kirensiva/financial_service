<?php

namespace App\Http\Resources;

use App\Models\Loan;
use Illuminate\Http\Resources\Json\JsonResource;

class LoanResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $loan = $this;
        return [
            'type' => Loan::class,
            'id' => $loan->id,
            'attributes' =>[
                'amount' => $loan->amount,
                'installment_frequency' => $loan->installment_frequency,
                'installment_duration' => $loan->installment_duration,
                'status' => $loan->status,
                'loan_installments' => LoanInstallmentResource::collection($this->installments),
                'user' => new UserResource($this->whenLoaded('user')),
            ]
        ];
    }
}
