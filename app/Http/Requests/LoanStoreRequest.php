<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;

class LoanStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|numeric',
            'installment_frequency' => 'required',
            'installment_duration' => 'required',
        ];
    }

    public function validationData()
    {
        $data = $this->all();
        return $data['data']['attributes'];
    }

    protected function failedValidation(Validator $validator)
    {
        throw (new ValidationException($validator));
                    // ->errorBag($this->errorBag);
                    // ->redirectTo($this->getRedirectUrl());
    }
}
