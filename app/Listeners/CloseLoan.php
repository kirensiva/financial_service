<?php

namespace App\Listeners;

use App\Events\InstallmentPaid;
use App\Service\Loan\Loan;

class CloseLoan
{
    /**
     * @var Loan
     */
    protected $loanService = null;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Loan $loanService)
    {
        $this->loanService = $loanService;
    }

    /**
     * Handle the event.
     *
     * @param  InstallmentPaid  $event
     * @return void
     */
    public function handle(InstallmentPaid $event)
    {
        $loaninstallment = $event->loaninstallment;
        $loan = $loaninstallment->loan;
        if ($this->loanService->isLoanClosed($loan)) {
            $loan->status = Loan::LOAN_STATUS_PAID;
            $loan->save();
        }
    }
}
