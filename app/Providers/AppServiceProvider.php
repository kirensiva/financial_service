<?php

namespace App\Providers;

use App\Repository\Eloquent\LoanInstallmentRepository;
use App\Repository\Eloquent\LoanRepository;
use App\Repository\Eloquent\LoanStatusRepository;
use Illuminate\Support\ServiceProvider;
use App\Repository\Eloquent\LoginRepository;
use App\Repository\Eloquent\UserRepository;
use App\Repository\LoanInstallmentRepositoryInterface;
use App\Repository\LoanRepositoryInterface;
use App\Repository\LoanStatusRepositoryInterface;
use App\Repository\LoginRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use App\Service\Loan\LoanInstallmentInterface;
use App\Service\Loan\WeeklyLoanInstallment;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(LoginRepositoryInterface::class, LoginRepository::class);
        $this->app->bind(LoanInstallmentInterface::class, WeeklyLoanInstallment::class);
        $this->app->bind(LoanRepositoryInterface::class, LoanRepository::class);
        $this->app->bind(LoanStatusRepositoryInterface::class, LoanStatusRepository::class);
        $this->app->bind(LoanInstallmentRepositoryInterface::class, LoanInstallmentRepository::class);
       
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
