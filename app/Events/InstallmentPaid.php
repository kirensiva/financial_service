<?php

namespace App\Events;

use App\Models\LoanInstallment;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class InstallmentPaid
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * The loaninstallment instance.
     *
     * @var \App\Models\LoanInstallment
     */
    public $loaninstallment;

    /**
     * Create a new event instance.
     *
     * @param  \App\Models\LoanInstallment  $loaninstallment
     * @return void
     */
    public function __construct(LoanInstallment $loaninstallment)
    {
        $this->loaninstallment = $loaninstallment;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
