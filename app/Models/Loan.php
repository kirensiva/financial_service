<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'amount',
        'installment_frequency',
        'installment_duration',
        'user_id',
        'status',
    ];

    public function installments()
    {
        return $this->hasMany(LoanInstallment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function status()
    {
        return $this->belongsTo(LoanStatus::class, 'loan_status_id');
    }
}
