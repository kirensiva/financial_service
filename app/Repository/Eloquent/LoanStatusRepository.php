<?php

namespace App\Repository\Eloquent;

use App\Models\LoanStatus;
use App\Repository\LoanRepositoryInterface;
use App\Repository\LoanStatusRepositoryInterface;

class LoanStatusRepository extends AbstractRepository implements LoanStatusRepositoryInterface
{
    public function getModel()
    {
        return LoanStatus::class;
    }
}
