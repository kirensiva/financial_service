<?php

namespace App\Repository\Eloquent;

use App\Models\Loan;
use App\Models\LoanInstallment;
use App\Repository\LoanInstallmentRepositoryInterface;

class LoanInstallmentRepository extends AbstractRepository implements LoanInstallmentRepositoryInterface
{
    public function getModel()
    {
        return LoanInstallment::class;
    }

    public function storeLoanInstallment(Loan $loan, $attr)
    {
        $installment = new LoanInstallment();
        $installment->installment_date =  $attr['installment_date'];
        $installment->amount = $attr['amount'];
        $installment->amount_paid = $attr['amount_paid'];
        $loan->installments()->save($installment);
    }
}
