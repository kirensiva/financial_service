<?php
namespace App\Repository\Eloquent;

use App\Repository\RepositoryInterface;
use Exception;
use Illuminate\Http\Request;

abstract class AbstractRepository implements RepositoryInterface
{
    protected $model = null;
    public function find(int $id)
    {
        $model = $this->createModel();
        return $model::find($id);
    }
    public function findAll()
    {
        $model = $this->createModel();
        return $model::all();
    }
    public function findOneBy($attributes)
    {
        return $this->findByAttributes($attributes)->first();
    }
    public function findBy($attributes)
    {
        return $this->findByAttributes($attributes)->get();
    }

    public function store($request)
    {
       return $this->save($request);
    }

    public function update($request)
    {
       return $this->save($request);
    }

    protected function save($formattedRequest) {
        $model = $formattedRequest['data']['type'] ?? null;
        $id = $formattedRequest['data']['id'] ?? null;
        $attributes = $formattedRequest['data']['attributes'] ?? null;
        if(empty($id)) {
            $model = $model::create($attributes);
        }else {
            $model = new  $model;
            $model = $model->find($id);
            $model->update($attributes);
        }
       
        $model->save();
        return $model;
    }

    protected function createModel()
    {
        $model = $this->getModel();
        if (empty($model)) {
            throw new Exception('Empty model');
        }
        return new $model;
    }

    protected function findByAttributes($attributes)
    {
        $model = $this->createModel();
        if (empty($attributes)) {
            throw new Exception('Empty Attributes');
        }
        $model = $this->getModel()::where($attributes);
        return  $model;
    }

    abstract public function getModel();
}
