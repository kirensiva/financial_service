<?php

namespace App\Repository\Eloquent;

use App\Models\Loan;
use App\Repository\LoanRepositoryInterface;

class LoanRepository extends AbstractRepository implements LoanRepositoryInterface
{
    public function getModel()
    {
        return Loan::class;
    }
}
