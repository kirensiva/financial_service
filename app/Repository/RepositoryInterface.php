<?php
namespace App\Repository;

interface RepositoryInterface
{
    public function find(int $id);
    public function findAll();
    public function findOneBy(array $attributes);
    public function findBy(array $attributes);
    public function store(array $attributes);
    public function update(array $attributes);
}
