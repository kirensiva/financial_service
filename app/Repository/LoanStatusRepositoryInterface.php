<?php

namespace App\Repository;

interface LoanStatusRepositoryInterface extends RepositoryInterface
{
    public const LOAN_STATUS_PENDING = 'pending';
    public const LOAN_STATUS_APPROVED = 'approved';
    public const LOAN_STATUS_PAID = 'paid';     
}
