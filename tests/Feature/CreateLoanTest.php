<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Service\Loan\Loan;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;

class CreateLoanTest extends TestCase
{
    use RefreshDatabase;

    public function test_create_loan()
    {
        Sanctum::actingAs(User::factory()->create(), ['*']);
        $data = [
            'data' => [
                'type' => 'App\Models\Loan',
                "attributes" =>  [
                    "amount" => "10.00",
                    "installment_frequency" => "weekly",
                    "installment_duration" => "3"
                ]

            ]
        ];
        $response = $this->postJson('/api/loans', $data);
        $response->assertStatus(201);
        $response->assertJsonPath('data.attributes.status', Loan::LOAN_STATUS_PENDING);
    }

    public function test_create_loan_unauthorised() {
        $data = [
            'data' => [
                'type' => 'App\Models\Loan',
                "attributes" =>  [
                    "amount" => "10.00",
                    "installment_frequency" => "weekly",
                    "installment_duration" => "3"
                ]

            ]
        ];
        $response = $this->postJson('/api/loans', $data);
        $response->assertUnauthorized();
    }
}
