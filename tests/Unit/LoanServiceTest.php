<?php

namespace Tests\Unit;

use App\Events\InstallmentPaid;
use App\Models\Loan as ModelsLoan;
use App\Models\User;
use App\Service\Loan\Loan as LoanService;
// use PHPUnit\Framework\TestCase;
use Tests\TestCase;
use Mockery\MockInterface;
use App\Service\Loan\LoanInstallmentInterface;
use App\Repository\LoanRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Prophecy\Argument;
use App\Models\LoanInstallment;
use Illuminate\Support\Facades\Event;

class LoanServiceTest extends TestCase
{
    
    public function test_create_loan()
    {
        $this->prophet = new \Prophecy\Prophet;
        $attrs = [
            'data' => [
                'type' => 'App\Models\Loan',
                "attributes" =>  [
                    "amount" => "10.00",
                    "installment_frequency" => "weekly",
                    "installment_duration" => "3"
                ]

            ]
        ];
        $loan = $this->prophet->prophesize(ModelsLoan::class);
        $loan->refresh()->shouldBeCalled()->willReturn($loan->reveal());
        $user = $this->prophet->prophesize(User::class);
        Auth::shouldReceive('user')
        ->once()
        ->andReturn($user);

        $loanInstallment = $this->prophet->prophesize(LoanInstallmentInterface::class);
        $loanRepository = $this->prophet->prophesize(LoanRepositoryInterface::class);
        $loanRepository->store(Argument::any())->willReturn($loan->reveal());
        $loanInstallment->createInstallments($loan)->shouldBeCalled();
        $loanService = new LoanService($loanInstallment->reveal(), $loanRepository->reveal());
        $loanObj = $loanService->createLoan($attrs);

        $this->assertEquals($loanObj, $loan->reveal());
    }

    public function test_pay_installment() {
        
        $this->prophet = new \Prophecy\Prophet;
        $attrs = [
            'data' => [
                'type' => 'App\Models\Loan',
                "attributes" =>  [
                    "amount" => "10.00",
                    "installment_frequency" => "weekly",
                    "installment_duration" => "3"
                ]

            ]
        ];
        $loanInstallment = $this->prophet->prophesize(LoanInstallment::class);
        $loanInstallmentService = $this->prophet->prophesize(LoanInstallmentInterface::class);
        $loanInstallmentService->payInstallment($loanInstallment->reveal(), $attrs)->willReturn( $loanInstallment->reveal());
        $loanRepository = $this->prophet->prophesize(LoanRepositoryInterface::class);
        Event::fake();
        $loanService = new LoanService($loanInstallmentService->reveal(), $loanRepository->reveal());
        $actualInstallmentObj = $loanService->payInstallment($loanInstallment->reveal(), $attrs);
        Event::assertDispatched(InstallmentPaid::class);
        $this->assertEquals($actualInstallmentObj, $loanInstallment->reveal());
    }
}
