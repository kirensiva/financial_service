# Installation
 - Checkout https://gitlab.com/kirensiva/financial_service
 - cd financial_service
 - composer install
 - create database
 - Update .env file with db connection
 - php artisan key:generate
 - php artisan migrate
 - php artisan db:seed
 - php artisan serve
 - Download postman collection from   `financial_service\docs\FinancialService.postman_collection.json`
 - Call the login api
 - From the response get the `plainTextToken`
 - Use this token to call other apis (Bearer Token Auth)

## Users

Admin
admin@example.com
password

Customer
customer@example.com
password
