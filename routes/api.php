<?php

use App\Http\Controllers\LoanController;
use App\Http\Controllers\InstallmentController;
use App\Http\Controllers\LoanStatusController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/login', [App\Http\Controllers\Auth\ApiLoginController::class, 'login'])->name('login');
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:sanctum')->group(function () {
    Route::apiResource('loans', LoanController::class);
    // Route::apiResource('loans.status', LoanStatusController::class);
    Route::patch('loans/{loan}/relationships/status', [LoanStatusController::class, 'update']);
    Route::apiResource('installments', InstallmentController::class);
    Route::apiResource('loanStatus', LoanStatusController::class);
    // Route::apiResource('loans/{loan}/status', LoanStatusController::class);
    // Route::patch('/installment/payInstallment/{loanInstallment}', [InstallmentController::class, 'payInstallment']);
    // Route::patch('/installment/approveLoan/{loan}', [LoanController::class, 'approveLoan']);
});
